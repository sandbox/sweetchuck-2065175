<?php
/**
 * @file
 * Contains \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\image;

use Drupal\field\FieldInstanceConfigInterface;
use Drupal\file\Entity\File;
use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorFile;

/**
 * Class ImageFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter\image
 */
class ImageFormatter extends \Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter {

  use FieldItemSelectorFile {
    FieldItemSelectorFile::prepareView as fisePrepareView;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    $this->fisePrepareView($entities_items);

    // If there are no files specified at all, use the default.
    foreach ($entities_items as $items) {
      if ($items->isEmpty()) {
        // Add the default image if one is found.
        $default_image = $this->getFieldSetting('default_image');
        // If we are dealing with a configurable field, look in both
        // instance-level and field-level settings.
        if (empty($default_image['fid']) && $this->fieldDefinition instanceof FieldInstanceConfigInterface) {
          $default_image = $this->getFieldSettings('default_image');
        }

        if (!empty($default_image['fid']) && ($file = File::load($default_image['fid']))) {
          $items->setValue(array(array(
              'is_default' => TRUE,
              'alt' => $default_image['alt'],
              'title' => $default_image['title'],
              'width' => $default_image['width'],
              'height' => $default_image['height'],
              'entity' => $file,
              'target_id' => $file->id(),
            )));
        }
      }
    }
  }

}
