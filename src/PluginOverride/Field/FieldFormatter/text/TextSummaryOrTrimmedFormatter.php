<?php
/**
 * @file
 * Home of TextSummaryOrTrimmedFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\text;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Override the TextSummaryOrTrimmedFormatter to add item selector selector.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\text
 */
class TextSummaryOrTrimmedFormatter extends \Drupal\text\Plugin\Field\FieldFormatter\TextSummaryOrTrimmedFormatter {

  use FieldItemSelectorBase;

}
