<?php
/**
 * @file
 * Home of the TextDefaultFormatter class.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\text;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Override the original TextDefaultFormatter to add field item selector.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\text
 */
class TextDefaultFormatter extends \Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter {

  use FieldItemSelectorBase;

}
