<?php
/**
 * @file
 * Home of TextTrimmedFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\text;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Override the original TextTrimmedFormatter to add field item selector.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\text
 */
class TextTrimmedFormatter extends \Drupal\text\Plugin\Field\FieldFormatter\TextTrimmedFormatter {

  use FieldItemSelectorBase;

}
