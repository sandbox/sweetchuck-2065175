<?php
/**
 * @file
 * Home of OptionsDefaultFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\options;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class OptionsDefaultFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\options
 */
class OptionsDefaultFormatter extends \Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter {

  use FieldItemSelectorBase;

}
