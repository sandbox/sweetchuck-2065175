<?php
/**
 * @file
 * Home of OptionsKeyFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\options;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class OptionsKeyFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\options
 */
class OptionsKeyFormatter extends \Drupal\options\Plugin\Field\FieldFormatter\OptionsKeyFormatter {

  use FieldItemSelectorBase;

}
