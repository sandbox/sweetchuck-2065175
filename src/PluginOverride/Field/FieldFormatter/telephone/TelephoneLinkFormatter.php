<?php
/**
 * @file
 * Home of the item selector capable TelephoneLinkFormatter field formatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\telephone;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class TelephoneLinkFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter\telephone
 */
class TelephoneLinkFormatter extends \Drupal\telephone\Plugin\Field\FieldFormatter\TelephoneLinkFormatter {

  use FieldItemSelectorBase

}
