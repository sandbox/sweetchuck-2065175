<?php
/**
 * @file
 * Documentation missing.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\datetime;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class DateTimeDefaultFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\datetime
 */
class DateTimeDefaultFormatter extends \Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter {

  use FieldItemSelectorBase;

}
