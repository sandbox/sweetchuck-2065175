<?php
/**
 * @file
 * Documentation missing.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\datetime;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class DateTimePlainFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\datetime
 */
class DateTimePlainFormatter extends \Drupal\datetime\Plugin\Field\FieldFormatter\DateTimePlainFormatter {

  use FieldItemSelectorBase;

}
