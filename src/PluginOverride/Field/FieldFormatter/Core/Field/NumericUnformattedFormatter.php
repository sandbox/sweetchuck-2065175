<?php
/**
 * @file
 * Home of NumericUnformattedFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class NumericUnformattedFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field
 */
class NumericUnformattedFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericUnformattedFormatter {

  use FieldItemSelectorBase;

}
