<?php
/**
 * @file
 * Home of the item selector capable StringFormatter field formatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class StringFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\field
 */
class StringFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter {

  use FieldItemSelectorBase;

}
