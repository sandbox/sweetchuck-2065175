<?php

/**
 * @file
 * Home of IntegerFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class IntegerFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field
 */
class IntegerFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\IntegerFormatter {

  use FieldItemSelectorBase;

}
