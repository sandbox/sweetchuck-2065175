<?php
/**
 * @file
 * Home of the item selector capable MailToFormatter field formatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class MailToFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\field
 */
class MailToFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\MailToFormatter {

  use FieldItemSelectorBase;

}
