<?php

/**
 * @file
 * Home of DecimalFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class DecimalFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\Core\Field
 */
class DecimalFormatter extends \Drupal\Core\Field\Plugin\Field\FieldFormatter\DecimalFormatter {

  use FieldItemSelectorBase;

}
