<?php
/**
 * @file
 * Contains \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter.
 */

namespace Drupal\fise\Plugin\Field\FieldFormatter;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorFile;

/**
 * Class GenericFileFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter
 */
class GenericFileFormatter extends \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter {

  use FieldItemSelectorFile;

}
