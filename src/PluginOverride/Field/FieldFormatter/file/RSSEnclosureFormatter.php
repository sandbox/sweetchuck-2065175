<?php
/**
 * @file
 * Contains \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter.
 */

namespace Drupal\fise\Plugin\Field\FieldFormatter;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorFile;

/**
 * Class RSSEnclosureFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter
 */
class RSSEnclosureFormatter extends \Drupal\file\Plugin\Field\FieldFormatter\RSSEnclosureFormatter {

  use FieldItemSelectorFile;

}
