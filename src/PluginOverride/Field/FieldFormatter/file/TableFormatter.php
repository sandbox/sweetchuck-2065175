<?php
/**
 * @file
 * Contains \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter.
 */

namespace Drupal\fise\Plugin\Field\FieldFormatter;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorFile;

/**
 * Class TableFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter
 */
class TableFormatter extends \Drupal\file\Plugin\Field\FieldFormatter\TableFormatter {

  use FieldItemSelectorFile;

}
