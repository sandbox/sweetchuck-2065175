<?php
/**
 * @file
 * Contains \Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter.
 */

namespace Drupal\fise\Plugin\Field\FieldFormatter;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorFile;

/**
 * Class UrlPlainFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter
 */
class UrlPlainFormatter extends \Drupal\file\Plugin\Field\FieldFormatter\UrlPlainFormatter {

  use FieldItemSelectorFile;

}
