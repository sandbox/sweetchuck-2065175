<?php
/**
 * @file
 * Home of the field item selector capable LinkFormatter field formatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\link;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class LinkFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\link
 */
class LinkFormatter extends \Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter {

  use FieldItemSelectorBase

}
