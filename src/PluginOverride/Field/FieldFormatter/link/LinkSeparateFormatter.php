<?php
/**
 * @file
 * Home of the field item selector capable LinkFormatter field formatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\link;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase;

/**
 * Class LinkSeparateFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\link
 */
class LinkSeparateFormatter extends \Drupal\link\Plugin\Field\FieldFormatter\LinkSeparateFormatter {

  use FieldItemSelectorBase

}
