<?php
/**
 * @file
 * Home of EntityReferenceEntityFormatter
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\entity_reference;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorEntityReference;

/**
 * Class EntityReferenceEntityFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter\entity_reference
 */
class EntityReferenceEntityFormatter extends \Drupal\entity_reference\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter {

  use FieldItemSelectorEntityReference;

}
