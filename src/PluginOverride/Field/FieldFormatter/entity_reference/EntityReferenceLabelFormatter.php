<?php
/**
 * @file
 * Home of EntityReferenceLabelFormatter.
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\entity_reference;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorEntityReference;

/**
 * Class EntityReferenceLabelFormatter.
 *
 * @package Drupal\fise\PluginOverride\Field\FieldFormatter\entity_reference
 */
class EntityReferenceLabelFormatter extends \Drupal\entity_reference\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter {

  use FieldItemSelectorEntityReference;

}
