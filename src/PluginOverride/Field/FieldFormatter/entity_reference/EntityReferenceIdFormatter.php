<?php
/**
 * @file
 * EntityReferenceIdFormatter
 */

namespace Drupal\fise\PluginOverride\Field\FieldFormatter\entity_reference;

use Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorEntityReference;

/**
 * Class EntityReferenceIdFormatter.
 *
 * @package Drupal\fise\Plugin\Field\FieldFormatter\entity_reference
 */
class EntityReferenceIdFormatter extends \Drupal\entity_reference\Plugin\Field\FieldFormatter\EntityReferenceIdFormatter {

  use FieldItemSelectorEntityReference;

}
