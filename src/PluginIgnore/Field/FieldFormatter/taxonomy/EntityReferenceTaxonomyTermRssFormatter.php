<?php

namespace Drupal\fise\Plugin\Field\FieldFormatter\taxonomy;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Field\FieldInterface;

/**
 * Override EntityReferenceTaxonomyTermRssFormatter to add selector capability.
 *
 * @FieldFormatter(
 *   id = "entity_reference_rss_category",
 *   label = @Translation("RSS category"),
 *   description = @Translation("Display reference to taxonomy term in RSS."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   settings = {
 *     "fise" = {
 *       "rules" = array(),
 *       "maximum" = 0,
 *       "maximum_percent" = 0,
 *       "maximum_before_end" = 0,
 *       "order_reversed" = 0,
 *       "order_as_it_is" = 0,
 *     }
 *   }
 * )
 */
class EntityReferenceTaxonomyTermRssFormatter extends \Drupal\taxonomy\Plugin\Field\FieldFormatter\EntityReferenceTaxonomyTermRssFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = parent::settingsForm($form, $form_state);

    $context = array(
      'formatter' => $this,
      'field' => $this->fieldDefinition,
    );
    fise_formatter_settings_form($element, $form_state, $context);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $context = array(
      'formatter' => $this,
      'field' => $this->fieldDefinition,
    );
    fise_formatter_settings_summary($summary, $context);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(EntityInterface $entity, $langcode, FieldInterface $items) {
    $settings = $this->getSetting('fise');

    $singleton = $this->fieldDefinition->getFieldCardinality() == 1;
    if ($singleton || fise_formatter_settings_is_empty($settings)) {
      return parent::viewElements($entity, $langcode, $items);
    }
    else {
      $deltas = fise_formatter_settings_get_deltas(count($items), $settings);
      $values_naked = $items->getValue();
      $values_ordered = array();
      foreach ($deltas as $delta) {
        if (isset($deltas[$delta])) {
          $values_ordered[] = $values_naked[$delta];
        }
      }
      $values = clone $items;
      $values->setValue($values_ordered);

      return parent::viewElements($entity, $langcode, $values);
    }
  }

}
