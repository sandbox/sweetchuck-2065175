<?php
/**
 * @file
 * Home of \Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase.
 */

namespace Drupal\fise\Extension\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Add the field item selector capability to a general field formatter.
 *
 * Compatible with FormatterInterface.
 *
 * @package Drupal\fise\Extension\Field\FieldFormatter
 */
trait FieldItemSelectorBase {

  /**
   * @var array
   */
  protected $fiseDeltas = NULL;

  /**
   * Defines the default settings for this plugin.
   *
   * @see \Drupal\Core\Field\PluginSettingsInterface::defaultSettings()
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + array(
      'fise' => fise_formatter_settings_create_empty(),
    );
  }

  /**
   * Returns a form to configure settings for the formatter.
   *
   * @see Drupal\Core\Field\FormatterInterface::settingsForm()
   */
  public function settingsForm(array $form, array &$form_state) {
    $element = parent::settingsForm($form, $form_state);
    fise_formatter_settings_form($element, $form_state, $this, $this->fieldDefinition);

    return $element;
  }

  /**
   * Returns a short summary for the current formatter settings.
   *
   * @see Drupal\Core\Field\FormatterInterface::settingsSummary()
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    fise_formatter_settings_summary($summary, $this, $this->fieldDefinition);

    return $summary;
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @see Drupal\Core\Field\FormatterInterface::viewElemets()
   */
  public function viewElements(FieldItemListInterface $items) {
    $settings = $this->getSetting('fise');
    $singleton = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1;
    if ($singleton || fise_formatter_settings_is_empty($settings)) {
      return parent::viewElements($items);
    }

    $deltas = $this->fiseGetDeltas($items, $settings);
    $values_naked = $items->getValue();
    $values_ordered = array();
    foreach ($deltas as $delta) {
      if (isset($values_naked[$delta])) {
        $values_ordered[] = $values_naked[$delta];
      }
    }
    $items_clone = clone $items;
    $items_clone->setValue($values_ordered);

    return parent::viewElements($items_clone);
  }

  /**
   * Sets the settings for the plugin.
   *
   * @see Drupal\Core\Field\PluginSettingsInterface::setSettings()
   */
  public function setSettings(array $settings) {
    if (isset($settings['fise'])) {
      $settings['fise'] = $this->setSettingsSanitize($settings['fise']);
    }

    return parent::setSettings($settings);
  }

  /**
   * Sets the value of a setting for the plugin.
   *
   * @see Drupal\Core\Field\PluginSettingsInterface::setSetting()
   */
  public function setSetting($key, $value) {
    if ($key == 'fise') {
      $value = $this->setSettingsSanitize($value);
    }

    return parent::setSetting($key, $value);
  }

  protected function setSettingsSanitize(array $settings) {
    $settings = fise_formatter_settings_sanitize($this->fieldDefinition->getFieldStorageDefinition()->getCardinality(), $settings);
    unset($settings['rules']['new']);

    return $settings;
  }

  /**
   * @param FieldItemListInterface $items
   * @param array $settings
   *
   * @return array
   */
  protected function fiseGetDeltas(FieldItemListInterface $items, array $settings) {
    if ($this->fiseDeltas === NULL) {
      $this->fiseDeltas = fise_formatter_settings_get_deltas($items->count(), $settings);
    }

    return $this->fiseDeltas;
  }

}
