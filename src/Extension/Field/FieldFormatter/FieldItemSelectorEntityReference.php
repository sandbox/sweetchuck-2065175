<?php
/**
 * @file
 * Home of \Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase.
 */

namespace Drupal\fise\Extension\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Add the field item selector capability to a general field formatter.
 *
 * Compatible with FormatterInterface.
 *
 * @package Drupal\fise\Extension\Field\FieldFormatter
 */
trait FieldItemSelectorEntityReference {

  use FieldItemSelectorBase;

  /**
   * {@inheritdoc}
   */
  protected function fiseGetDeltas(FieldItemListInterface $items, array $settings) {
    $available_deltas = array();
    foreach ($items as $delta => $item) {
      if ($item->access) {
        $available_deltas[] = $delta;
      }
    }

    $visible_deltas = fise_formatter_settings_get_deltas(count($available_deltas), $settings);

    return array_intersect_key($available_deltas, $visible_deltas);
  }

}
