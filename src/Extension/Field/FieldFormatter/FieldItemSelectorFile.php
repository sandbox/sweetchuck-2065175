<?php
/**
 * @file
 * Home of \Drupal\fise\Extension\Field\FieldFormatter\FieldItemSelectorBase.
 */

namespace Drupal\fise\Extension\Field\FieldFormatter;

use Drupal\file\Entity\File;

/**
 * Add the field item selector capability to a general field formatter.
 *
 * Compatible with FormatterInterface.
 *
 * @package Drupal\fise\Extension\Field\FieldFormatter
 */
trait FieldItemSelectorFile {

  use FieldItemSelectorBase;

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    // Remove files specified to not be displayed.
    $file_ids = array();
    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        if ($item->isDisplayed() && !empty($item->target_id)) {
          // Load the files from the files table.
          $file_ids[] = $item->target_id;
        }
      }
    }

    $file_ids = $this->fiseSelectItems($file_ids);
    if ($file_ids) {
      $files = File::loadMultiple($file_ids);

      foreach ($entities_items as $items) {
        foreach ($items as $item) {
          // If the file does not exist, mark the entire item as empty.
          if (!empty($item->target_id)) {
            $item->entity = isset($files[$item->target_id]) ? $files[$item->target_id] : NULL;
          }
        }
      }
    }
  }

  /**
   * @param int[] $file_ids
   *
   * @return int[]
   */
  protected function fiseSelectItems(array $file_ids) {
    $settings = $this->getSetting('fise');
    $singleton = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1;
    if ($singleton || fise_formatter_settings_is_empty($settings)) {
      return $file_ids;
    }

    return array_intersect_key(
      $file_ids,
      fise_formatter_settings_get_deltas(count($file_ids), $settings)
    );
  }

}
